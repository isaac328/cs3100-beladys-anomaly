#include<iostream>
#include<queue>
#include<stdlib.h> 
#include<vector>
#include <unordered_set>

int main()
{
	//seed the random generator
	srand(time(0));
	
	//fifo queue for pages in memory
	std::queue<int> queue;
	
	//store the pages in the reference string
	std::vector<int> refString(1000);
	
	//array for checking if pages are in memory
	//make it 251 just for convienence so we can use
	//the actual page number instead of subtracting 1
	bool checkQueue[251];
	
	//initialize array
	for(int i = 0; i < 251; i++)
		checkQueue[i] = false;
	
	//track the number of anomalies	
	int anomalies = 0;
	
	//100 sequences
	for(int sequence = 0; sequence < 100; sequence++)
	{
		//set the previous faults number
		int previousFaults = 100000;
		
		//generate the reference string
		for (int i = 0; i < 1000; i++)
		{
			refString.insert(refString.begin(), rand() % 250 + 1);
		}
	
		//keep track of faults
		int faults = 0;
	
		//go through all the frame sizes for this string
		for (int frameSize = 1; frameSize <= 100; frameSize++)
		{
			//go through the reference string
			for (int page : refString)
			{
				//check if the page is already in memory
				bool contains = checkQueue[page];
	
				//if its not in memory
				if (!contains)
				{
					//if the queue(memory) is already full
					if (queue.size() >= frameSize)
					{
						//erase the oldest element from memory
						checkQueue[queue.front()] = false;
						queue.pop();
					}
					//insert the new element into memory
					queue.push(page);
					checkQueue[page] = true;
	
					//increase faults
					faults++;
				}
			}
	
			//empty memory to reset for next frame
			while (!queue.empty())
			{
				checkQueue[queue.front()] = false;
				queue.pop();
			}
			
			
			//check if we had more faults in this frame than we did in the previous frame
			//if so then we found an anomaly
			if(faults > previousFaults)
			{
				std::cout << "Anomoly Discovered!" << std::endl;
				std::cout << "Sequence: " << sequence + 1 << std::endl;
				std::cout << "Page Faults: " << previousFaults << " @ Frame Size: " << frameSize-1 << std::endl;
				std::cout << "Page Faults: " << faults << " @ Frame Size: " << frameSize << std::endl << std::endl;
				anomalies++;
			}
			
			//reset the previous faults to our current number of faults
			previousFaults = faults;
			
			//reset page faults
			faults = 0;
		}
		//delete the reference string after we are done going through all the frames for this string
		refString.clear();
	}
	
	std::cout << "Anomaly detected " << anomalies << " times" << std::endl;
	
	return 0;
}
